# Pokédex python

Projet scolaire de développement d'un pokédex en python et en utilisant l'api PokéApi.
Les consignes étaient d'avoir une interface graphique qui affichait l'image, le nom et les types d'un pokémon après avoir entré son nom ou son numéro. On pouvait ensuite ajouter ce pokémon a notre équipe. L'équipe devait compter maximum 6 pokémons, qu'il était possible de retirer à volonté.

Pour ce projet, j'ai utilisé la librairie graphique Tkinter ainsi que la librairie Pillow, pour afficher les images et la librairie requests pour gérer l'API. 

## Installation des librairies
Pour installer toutes les librairies, utiliser la commande "py -m pip install -r requirement.txt"

## Utilisation du Pokedex
L'utilisation du Pokedex est simple. Une fois lancé, on rentre l'id d'un pokemon ou son nom anglais et on clique sur le bouton chercher.

On peut ensuite chercher un nouveau pokemon ou cliqué sur les fleches sur les côtés pour afficher les pokemons précédents ou suivants.

En cliquant sur le bouton +, on ajoute le pokemon à son équipe. Celle-ci est disponible en cliquant sur l'onglet "Team".
Depuis cet onglet, on peut supprimer un pokemon de son équipe en cliquant sur la croix à côté de son nom ou vider l'équipe entièrement en cliquant sur le bouton "Supprimer toute l'équipe".
