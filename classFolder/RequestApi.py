import requests

from classFolder.Error import Error
from classFolder.Pokemon import Pokemon


class RequestApi:
    url_api_pokemon = 'https://pokeapi.co/api/v2/pokemon/'
    # Nombre max de pokemon de l'api
    nb_max_pokemon = 905
    error_handler = Error()

    # Récuperation des données des pokemons depuis l'api
    def get_pokemon_data(self, url_pokemon):
        response = requests.get(self.url_api_pokemon + url_pokemon)
        if response.status_code == 200:
            data = response.json()
            image = data['sprites']['front_default']

            id_pokemon = data["id"]

            trad_name = requests.get(data["species"]["url"])
            trad_name = trad_name.json()

            name_pokemon = ''
            for name in trad_name["names"]:
                if name["language"]["name"] == "fr":
                    name_pokemon = name["name"]

            if name_pokemon == '':
                name_pokemon = data["name"]

            height_pokemon = data["height"] / 10
            weight_pokemon = data["weight"] / 10

            types_pokemon = []
            for type in data["types"]:
                trad_type = requests.get(type["type"]["url"])
                trad_type = trad_type.json()

                for type_traduit in trad_type["names"]:
                    if type_traduit["language"]["name"] == "fr":
                        types_pokemon.append(type_traduit["name"])

            if not types_pokemon:
                for type in data["types"]:
                    types_pokemon.append(type["type"]["name"])

            stats_pokemon = {
                "hp": 0,
                "attack": 0,
                "defense": 0,
                "special-attack": 0,
                "special-defense": 0,
                "speed": 0,
            }
            for stat in data["stats"]:
                stats_pokemon[stat['stat']["name"]] = stat["base_stat"]

            return Pokemon(id_pokemon, name_pokemon, image, height_pokemon, weight_pokemon, types_pokemon,
                           stats_pokemon)
        elif response.status_code == 404:
            self.error_handler.error_recherche()
