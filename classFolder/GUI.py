from io import BytesIO
from tkinter import Button, Text, Label, Frame, Tk
from tkinter.ttk import Notebook

import requests
from PIL import ImageTk, Image as Imagepil, Image

from classFolder.Error import Error
from classFolder.RequestApi import RequestApi


class GUI:
    # Créer l'interface graphique
    def __init__(self, poketeam):
        self.poketeam = poketeam
        self.window = Tk()
        self.window.geometry("350x760")
        self.window.title("Pokedex")
        self.window.config(padx=10, pady=10)
        self.error = Error()

        poke_image = Imagepil.open('img/pokedex_font2.png')

        poke_image = poke_image.resize((350, 750))
        self.pokeBackground = ImageTk.PhotoImage(poke_image)

        self.tabControl = Notebook(self.window)

        self.tab1 = Frame(self.tabControl)
        self.tab2 = Frame(self.tabControl)

        Label(self.tab1, image=self.pokeBackground).place(x=0, y=0, relwidth=1, relheight=1)

        self.tabControl.add(self.tab1, text='Pokedex')
        self.tabControl.add(self.tab2, text='Team')

        self.tabControl.pack(expand=1, fill="both")

        self.pokemon_image = Label(self.tab1)
        self.pokemon_image.grid(row=2, column=2, pady=10)

        self.pokemon_information = Label(self.tab1)
        self.pokemon_information.config(font=("Arial", 20))
        self.pokemon_information.grid(row=1, column=2, pady=(150, 10))

        self.label_id_name = Label(self.tab1, text="Id ou nom anglais")
        self.label_id_name.config(font=("Arial", 20))
        self.label_id_name.grid(row=4, column=2, pady=10)

        self.text_id_name = Text(self.tab1, height=1, width=14)
        self.text_id_name.config(font=("Arial", 20))
        self.text_id_name.grid(row=6, column=2, pady=10, padx=16)

        self.btn_load = Button(self.tab1, text="Chercher", command=lambda: self.load_pokemon())
        self.btn_load.config(font=("Arial", 20))
        self.btn_load.grid(row=7, column=2, pady=10)

        self.btn_prec = Button(self.tab1, text="<", command=lambda: self.load_pokemon())
        self.btn_prec.config(font=("Arial", 20))
        self.btn_prec.grid(row=8, column=1)
        self.btn_suiv = Button(self.tab1, text=">", command=lambda: self.load_pokemon())
        self.btn_suiv.config(font=("Arial", 20))
        self.btn_suiv.grid(row=8, column=3)

        # pokemon_hp = Label(tab1)
        # pokemon_hp.config(font=("Arial", 20))
        # pokemon_hp.grid(row=1, column=1)
        #
        # pokemon_atk = Label(tab1)
        # pokemon_atk.config(font=("Arial", 20))
        # pokemon_atk.grid(row=2, column=1)
        #
        # pokemon_defense = Label(tab1)
        # pokemon_defense.config(font=("Arial", 20))
        # pokemon_defense.grid(row=3, column=1)
        #
        # pokemon_special_attack = Label(tab1)
        # pokemon_special_attack.config(font=("Arial", 20))
        # pokemon_special_attack.grid(row=1, column=3)
        #
        # pokemon_special_defense = Label(tab1)
        # pokemon_special_defense.config(font=("Arial", 20))
        # pokemon_special_defense.grid(row=2, column=3)
        #
        # pokemon_speed = Label(tab1)
        # pokemon_speed.config(font=("Arial", 20))
        # pokemon_speed.grid(row=3, column=3)

        self.poketeam.display_team(self.tab2)

    # Charge le pokemon et l'affiche
    def load_pokemon(self, id_pokemon="1", mode_load=0):
        req_api = RequestApi()
        if mode_load == 1:
            if id_pokemon == req_api.nb_max_pokemon:
                pokemon_id = "1"
            else:
                pokemon_id = str(id_pokemon + 1)
            pokedata = req_api.get_pokemon_data(pokemon_id.strip().lower())
        elif mode_load == -1:
            if id_pokemon == 1:
                pokemon_id = str(req_api.nb_max_pokemon)
            else:
                pokemon_id = str(id_pokemon - 1)
            pokedata = req_api.get_pokemon_data(pokemon_id.strip().lower())
        else:
            pokemon_id = self.text_id_name.get(1.0, "end-1c")
            if pokemon_id == "":
                pokemon_id = id_pokemon
            pokedata = req_api.get_pokemon_data(pokemon_id.strip().lower())

        if pokedata is not None:
            if pokedata.image is not None:
                image = requests.get(pokedata.image)
                image = ImageTk.PhotoImage(Imagepil.open(BytesIO(image.content)))
            else:
                image = Image.open('img/interrogation.jpg')
                image = ImageTk.PhotoImage(image.resize((96, 96)))

            self.pokemon_image.config(image=image)
            self.pokemon_image.image = image

            self.pokemon_information.config(text=f"Nom: {pokedata.name}\nId: {pokedata.id}")
            self.label_id_name.config(
                text=" - ".join(
                    [t for t in pokedata.types]) + f"\ntaille: {pokedata.height} m, \npoids: {pokedata.weight} kg")

            # pokemon_hp.config(text=pokedata["hp"])
            # pokemon_atk.config(text=pokedata["attack"])
            # pokemon_defense.config(text=pokedata["defense"])
            # pokemon_special_attack.config(text=pokedata["special-attack"])
            # pokemon_special_defense.config(text=pokedata["special-defense"])
            # pokemon_speed.config(text=pokedata["speed"])

            self.add_button(pokedata)

    # Ajoute les boutons fonctionnels lorsqu'un pokemon est chargé
    def add_button(self, pokemon):
        btn_choose = Button(self.tab1, text="+", command=lambda: self.add_team_pokemon(pokemon, self.poketeam))
        btn_choose.config(font=("Arial", 20))
        btn_choose.grid(row=8, column=2)

        btn_prec = Button(self.tab1, text="<", command=lambda: self.load_pokemon(pokemon.id, -1))
        btn_prec.config(font=("Arial", 20))
        btn_prec.grid(row=8, column=1)
        btn_suiv = Button(self.tab1, text=">", command=lambda: self.load_pokemon(pokemon.id, 1))
        btn_suiv.config(font=("Arial", 20))
        btn_suiv.grid(row=8, column=3)

    # Ajoute un pokemon a l'équipe
    def add_team_pokemon(self, pokemon, poketeam):
        if poketeam.message_ajout():
            poketeam.add_pokemon(pokemon=pokemon)
            poketeam.display_team(self.tab2)
