import tkinter.messagebox
from io import BytesIO
from tkinter import Label, Button
import requests
from PIL import Image as Imagepil, ImageTk


class Team:

    def __init__(self):
        self.poke_team = []

    # Ajoute un pokemon à l'équipe
    def add_pokemon(self, pokemon):
        if len(self.poke_team) != 6:
            self.poke_team.append(pokemon)

    # Affiche le message de confirmation pour l'ajout d'un pokemon à l'équipe
    def message_ajout(self):
        if len(self.poke_team) != 6:
            return tkinter.messagebox.askyesno(title="Ajout d'un pokemon",
                                               message="Voulez-vous ajouter ce pokemon à votre équipe ?")
        else:
            tkinter.messagebox.showwarning(title="Equipe pleine", message="Votre équipe est pleine ! \nVous aller "
                                                                          "devoir vous séparer d'un pokemon")
            return False

    # Affiche l'équipe
    def display_team(self, onglet):
        for widget in onglet.winfo_children():
            widget.destroy()

        btn_delete_all = Button(onglet, text="Supprimer toute l'équipe", command=lambda: self.delete_all(onglet))
        btn_delete_all.grid(row=0, column=0, pady=(10, 10))

        i = 0
        for p in self.poke_team:
            if i == 0:
                btn_delete_1 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[0],
                                                                                                 onglet))
                btn_delete_1.grid(row=1, column=2, padx=(5, 100))
            elif i == 1:
                btn_delete_2 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[1],
                                                                                                 onglet))
                btn_delete_2.grid(row=2, column=2, padx=(5, 100))
            elif i == 2:
                btn_delete_3 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[2],
                                                                                                 onglet))
                btn_delete_3.grid(row=3, column=2, padx=(5, 100))
            elif i == 3:
                btn_delete_4 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[3],
                                                                                                 onglet))
                btn_delete_4.grid(row=4, column=2, padx=(5, 100))
            elif i == 4:
                btn_delete_5 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[4],
                                                                                                 onglet))
                btn_delete_5.grid(row=5, column=2, padx=(5, 100))
            elif i == 5:
                btn_delete_6 = Button(onglet, text="X", command=lambda: self.delete_this_pokemon(self.poke_team[5],
                                                                                                 onglet))
                btn_delete_6.grid(row=6, column=2, padx=(5, 100))

            lb = Label(onglet)
            lb.config(text=p.name)
            lb.grid(row=i + 1, column=1, padx=(0, 5))

            image = Imagepil.open(BytesIO(requests.get(p.image).content))
            img = ImageTk.PhotoImage(image)
            team_pokemon_image = Label(onglet)
            team_pokemon_image.config(image=img)
            team_pokemon_image.image = img
            team_pokemon_image.grid(row=i + 1, column=0)
            i += 1

    # Easter egg que je n'ai pas réussi à finir
    def oeuf_de_paque_team_ramoloss(self, onglet):
        i = 0
        odp_image = Imagepil.open('img/slowpoke.jpg')
        background = ImageTk.PhotoImage(odp_image)
        for pokemon in self.poke_team:
            if pokemon.name in ["Ramoloss", "Slowpoke"]:
                i += 1
        if i == 6:
            onglet.config(image=background)
            onglet.image = background

    # Easter egg lors de la suppression d'un ramoloss de l'équipe
    def oeuf_de_paque_suppression_ramoloss(self):
        res = tkinter.messagebox.askyesno(title="Suppression d'un ramolosse",
                                          message="Attention ! Suppression d'un ramolosse détecté !\n"
                                                  "Êtes-vous sûr de ce que vous faites ?")
        if res:
            res = tkinter.messagebox.askyesno(title="Verification de la suppression d'un ramolosse",
                                              message="Êtes-vous certain de vouloir supprimer ce ramolosse de "
                                                      "votre équipe ?")
            if res:
                res = tkinter.messagebox.askyesno(title="Elimination d'un ramolosse",
                                                  message="Êtes-vous au courant qu'après avoir tuer ce "
                                                          "ramolosse, il sera à jamais trop tard pour le "
                                                          "récuperer ?")
                if res:
                    res = tkinter.messagebox.showinfo(title="Honte à vous", message="Cliquer sur OK si vous êtes un "
                                                                                    "monstre sans coeur")
        return res

    # Suppression d'un pokemon de l'équipe
    def delete_this_pokemon(self, pokemon, onglet):
        supp = True
        if pokemon.name in ["Ramoloss", "Slowpoke"]:
            supp = self.oeuf_de_paque_suppression_ramoloss()
        else:
            supp = tkinter.messagebox.askyesno(title="Suppression d'un pokemon",
                                               message=f"Êtes-vous sûr de vouloir retirer {pokemon.name} "
                                                       f"de votre équipe ?")
        if supp:
            self.poke_team.remove(pokemon)
            self.display_team(onglet)

    # Suppression de toute l'équipe de pokemon
    def delete_all(self, onglet):
        supp = tkinter.messagebox.askyesno(title="Suppression des pokemons de votre équipe",
                                           message="Êtes-vous sûr de vouloir retirer tous les pokemons"
                                                   " de votre équipe ?")
        for p in self.poke_team:
            if p.name in ["Ramoloss", "Slowpoke"] and supp:
                supp = self.oeuf_de_paque_suppression_ramoloss()
                if supp:
                    self.poke_team.clear()
                    self.display_team(onglet)

        if supp:
            self.poke_team.clear()
            self.display_team(onglet)
