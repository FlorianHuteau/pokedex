class Pokemon:
    def __init__(self, id, name, image, height, weight, types, stats):
        self.id = id
        self.stats = stats
        self.types = types
        self.weight = weight
        self.height = height
        self.name = name
        self.image = image


