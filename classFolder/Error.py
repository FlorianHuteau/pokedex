import tkinter.messagebox


class Error:

    # Gère les erreurs de recherche
    def error_recherche(self):
        tkinter.messagebox.showwarning(title="Erreur lors de la recherche",
                                       message="Une erreur est survenu lors de la recherche.\n"
                                               "L'id ou le nom du pokemon n'existe peut-être pas ou a été mal "
                                               "ortografié.")
